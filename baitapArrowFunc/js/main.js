

const colors = ["pallet", "viridian", "pewter", "cerulean", "vermillion", "lavender", "celadon", "saffron", "fuschia", "cinnabar"];
const renderButton = () => {
    let contentHTML = '';
    for (let i = 0; i < colors.length; i++) {
        const color = colors[i];
        contentHTML +=
            `<button id="button-house" class="active color-button ${color}"
        onclick="changeColor('${color}')"></button>`;
    }
    document.getElementById("colorContainer").innerHTML = contentHTML;
};
renderButton();

let changeColor = (color) => {
    document.getElementById("house").classList.add(color);
};


//active
var tabLinks = document.querySelectorAll(".tablinks");
var tabContent = document.querySelectorAll(".tabcontent");

tabLinks.forEach(function (el) {
    el.addEventListener("click", openTabs);
});


function openTabs(el) {
    var btn = el.currentTarget; // lắng nghe sự kiện và hiển thị các element
    var electronic = btn.dataset.electronic; // lấy giá trị trong data-electronic

    tabContent.forEach(function (el) {
        el.classList.remove("active");
    }); //lặp qua các tab content để remove class active

    tabLinks.forEach(function (el) {
        el.classList.remove("active");
    }); //lặp qua các tab links để remove class active

    document.querySelector("#" + electronic).classList.add("active");
    // trả về phần tử đầu tiên có id="" được add class active

    btn.classList.add("active");
    // các button mà chúng ta click vào sẽ được add class active
}









